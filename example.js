const net = require('net');
const StratumProxy = require('./index');

const poolAddress = process.env.POOL_ADDRESS || 'us2.ethermine.org';
const poolPort = process.env.POOL_PORT || 4444;
const minerName = process.env.MINER_NAME || 'proxy';
const address = process.env.MINER_ADDRESS || '2505c431c48103b2bdb22a50a2c8762840fc3142';
const listenPort = process.env.LISTEN_PORT || 3333
const poolPassword = process.env.POOL_PASSWORD || 'x'
const app = new StratumProxy({
	poolAddress,
	poolPort,
	minerName,
	address,
	poolPassword
});

app.on('log', ({ response }) => console.log(response));
app.on('data', ({ response }) => console.log(response));
app.on('devFeeDetected', ({ response }) => console.log(response));

app.listen(listenPort);
